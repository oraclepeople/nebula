#!/bin/bash

#
# The script creates a MS SQL server on Azure VMs
#

# Resource created:
# mysqlvm Microsoft.SqlVirtualMachine/SqlVirtualMachines
# shutdown-computevm-mysqlvm Microsoft.DevTestLab/schedules
# mysqlvm Microsoft.Compute/virtualMachines
# mysqlvm410 Microsoft.Network/networkInterfaces
# rg-sqlvm-vnet Microsoft.Network/virtualNetworks
# mysqlvm-ip Microsoft.Network/publicIpAddresses
# mysqlvm-nsg Microsoft.Network/networkSecurityGroups
# mysqlvm_DataDisk_0 Microsoft.Compute/disks
#  az vm image list --all --publisher MicrosoftSQLServer --sku sqldev  --output table
#  {
#      "offer": "sql2019-ws2019",
#      "publisher": "MicrosoftSQLServer",
#      "sku": "sqldev",
#      "urn": "MicrosoftSQLServer:sql2019-ws2019:sqldev:15.0.211109",
#      "version": "15.0.211109"
#    }
#
# Size to choose:
#   Standard_DS1_v2 : vCpu=1, RAM=3.5GiB £79.94/month; 
#   Standard_DS2_v2 : vCpu=2, RAM=7GiB £159.87/month;
#   Standard_B2ms: vCpu=2; RAM=8,  £55.55/month
#   Standard_B2s: vCpu=2; RAM=4,  £27.01/month
# Disk: 

function create_vnet_subnet {
    SECONDS=0
    echo -e "\nCreating $SQLVM_VNET with $SQLVM_SUBVNET..."
    az network vnet create \
    -n $SQLVM_VNET \
    -g $SQLVM_RESOURCE_GROUP \
    -l "$LOCATION" \
    --address-prefixes 10.0.0.0/16

    az network vnet subnet create \
    -n $SQLVM_SUBVNET \
    -g $SQLVM_RESOURCE_GROUP \
    --vnet-name $SQLVM_VNET \
    --address-prefixes 10.0.0.0/24 \

    echo -e "\ncreation completed.  Timed used $SECONDS seconds"
}

function create_nsg {
    SECONDS=0

    echo -e "\nCreating $SQLVM_NSG..."
    az network nsg create \
    -n $SQLVM_NSG -g $SQLVM_RESOURCE_GROUP \
    -l "$LOCATION"

    echo -e "\nCreating nsg rule for Azure Backup..."
    priority=1000
    for n in AzureActiveDirectory Storage AzureBackup; do
        az network nsg rule create \
        -n "allow_${n}_outbound" \
        --nsg-name $SQLVM_NSG \
        --priority "$priority" \
        -g $SQLVM_RESOURCE_GROUP \
        --access Allow \
        --destination-address-prefixes $n \
        --direction Outbound \
        --protocol Tcp

        priority=$(($priority + 100 ))
    done

    echo -e "\nCreating nsg rule for RDP..."
    az network nsg rule create \
    -n "RDP" \
    --nsg-name $SQLVM_NSG \
    --priority 300 \
    -g $SQLVM_RESOURCE_GROUP \
    --access Allow \
    --destination-port-ranges 3389 \
    --direction Inbound \
    --protocol Tcp \
    --source-address-prefixes "*" \
    --source-port-ranges "*"

    # Need to put Service field "MS SQL"  in 
    echo -e "\nCreating nsg rule for MS SQL..."
    az network nsg rule create \
    -n "default-allow-sql" \
    --nsg-name $SQLVM_NSG \
    --priority $priority \
    -g $SQLVM_RESOURCE_GROUP \
    --access Allow \
    --destination-port-ranges 1443 \
    --direction Inbound \
    --protocol Tcp \
    --source-address-prefixes "*" \
    --source-port-ranges "*"
}

function create_vm {
    SECONDS=0
    echo  -e "\nCreating VM for SQL; Must use image of MicrosoftSQLServer instead of Windows Server..."
    echo "DNS name of the server: sqlvm-6666.ukwest.cloudapp.azure.com; Disk size > 127 GB"
    echo "Disk=P15, Size=256 GB, IOPS/disk=1,100, Throughput=125MB/s"
    echo "Size: Standard_B2s vCpu=2 RAM=4  £27.01/month"
    echo "Auto shutdown 21:30"
    az vm create \
    -n $SQLVM \
    -g $SQLVM_RESOURCE_GROUP \
    --image "MicrosoftSQLServer:sql2019-ws2019:sqldev:15.0.211109" \
    --nsg $SQLVM_NSG \
    --nsg-rule RDP \
    --public-ip-address $SQLVM_PUBLIC_IP \
    --public-ip-address-dns-name $SQLVM_DNS_NAME \
    --public-ip-sku Standard \
    --os-disk-name $SQLVM_OS_DISK \
    --os-disk-size-gb 256 \
    --vnet-name $SQLVM_VNET \
    --subnet $SQLVM_SUBVNET \
    --admin-username $SQLVM_WINDOWS_ADMIN \
    --admin-password $PASSWORD \
    --size Standard_B2s \
    -l $LOCATION_ABBR

    az vm auto-shutdown \
    -g $SQLVM_RESOURCE_GROUP \
    -n $SQLVM  \
    --time 2130

    echo -e "\nVM Creation completed. Timed used $SECONDS seconds" 
}

function create_sqlvm {
    SECONDS=0
    echo -e "\nCreating SQLVM; It just registers SQL VM with SQL IaaS Extension (Windows)"
    az sql vm create \
    -n $SQLVM \
    -g $SQLVM_RESOURCE_GROUP \
    -l $LOCATION_ABBR \
    --license-type PAYG \
    --connectivity-type PUBLIC \
    --enable-auto-backup false \
    --image-sku Developer \
    --sql-auth-update-username $SQLVM_DB_ADMIN \
    --sql-auth-update-pwd $PASSWORD \
    --sql-mgmt-type Full

    echo -e "\nSQL VM creation completed. Timed used $SECONDS seconds" 
}

function create_recovery_vault {
    az backup vault create \
    -l $LOCATION_ABBR \
    --name $SQLVM_RECOVERY_VAULT \
    -g $SQLVM_RESOURCE_GROUP 
}

function create_storage_account {

    sku="Standard_LRS"

    echo -e "\nCreating Storage account..."
    echo "account name must be unique across all storage account names in Azure"
    echo "name must be lowcase and number only"
    az storage account create \
    -n $SQLVM_STORAGE_ACCOUNT \
    -g $SQLVM_RESOURCE_GROUP \
    -l $LOCATION_ABBR \
    --sku $sku

    echo "Creating $container on $STORAGE_ACCOUNT..."
    key=$(az storage account keys list --account-name $STORAGE_ACCOUNT -g $SQLVM_RESOURCE_GROUP -o json --query '[0].value' | tr -d '"')
    container="container$ID"

    az storage container create \
    -n $container \
    --account-key $key \
    --account-name $STORAGE_ACCOUNT
}


SCRIPT=$(realpath $0)
SCRIPTPATH=$(dirname $SCRIPT)

# Load the command variables
. $SCRIPTPATH/azure.env

# Contains sensitive info such as password, Azure subscription ID and network IP ranges
. $HOME/.ssh/azure-secreat

az group create -n $SQLVM_RESOURCE_GROUP -l "$LOCATION"

create_vnet_subnet

create_nsg

create_vm

create_sqlvm

create_recovery_vault