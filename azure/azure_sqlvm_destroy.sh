#!/bin/bash

function delete_sqlvm {

    echo -e "\nDelete SQL VM $SQLVM..." 
    SECONDS=0

    az sql vm delete \
    -n $SQLVM \
    -g $SQLVM_RESOURCE_GROUP \
    -y
    echo -e "\nSQL VM deletion completed. Timed used $SECONDS seconds" 
}

function delete_vm {

    SECONDS=0
    echo -e "\nDelete VM $SQLVM..." 
    az vm delete \
    -n $SQLVM \
    -g $SQLVM_RESOURCE_GROUP \
    -y
    echo -e "\nVM Deletion completed. Timed used $SECONDS seconds" 
}

function delete_recovery_vault {
    SECONDS=0
    # echo -e "\n Delete Recovery Services Vault..."
    # az backup vault backup-properties set \
    # -n recoveryvault \
    # -g rg-sqlvm \
    # --soft-delete-feature-state disable
    
    az backup vault delete  \
    --name $SQLVM_RECOVERY_VAULT \
    --resource-group $SQLVM_RESOURCE_GROUP  \
    --force --yes
    echo -e "\nDeletion completed. Timed used $SECONDS seconds" 
}

function delete_resource_group {
    echo -e "\nDelete resource group $SQLVM_RESOURCE_GROUP..."                    
    az group delete \
    -n $SQLVM_RESOURCE_GROUP \
    -y

    az group delete \
    -n NetworkWatcherRG \
    -y
}

function delete_storage_account {
    SECONDS=0
    echo -e "\nDelete storage account $STORAGE_ACCOUNT..."                    
    az storage account delete \
    -g $SQLVM_RESOURCE_GROUP \
    -n $SQLVM_STORAGE_ACCOUNT \
    -y
    echo -e "\nCompleted. Timed used $SECONDS seconds"
}

SCRIPT=$(realpath $0)
SCRIPTPATH=$(dirname $SCRIPT)

# Load the command variables
. $SCRIPTPATH/azure.env

# Contains sensitive info such as password, Azure subscription ID and network IP ranges
. $HOME/.ssh/azure-secreat

delete_sqlvm

delete_vm

delete_recovery_vault

delete_storage_account

delete_resource_group


