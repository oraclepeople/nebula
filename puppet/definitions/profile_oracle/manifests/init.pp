#
# Demonstrates how to use Puppet to dynamically generate Oracle database tnsnames.ora file.
# The tnsnames file contains the following entries:
#  1) Entries that points to every member host in a Dataguard configuration
#  2) Other entreis that is listed in individual host's yaml file
#
class  profile_oracle{

  # Load data from individual node
  $oracle_home = hiera('oracle::oracle_home')
  $service_name = hiera('oracle::service_name')  
  $oradb_port = hiera('oracle::oradb_port', 1521)
  $dataguard_tag_name = hiera('oracle::dataguard_tag_name')
  
  $dg_hosts = sort(query_nodes("Class[profile_oracle]{ tag = '${dataguard_tag_name}' }", 'hostname'))
  
  class {'oracle::tnsnames':
    config_path            => "${oracle_home}/network/admin/tnsnames.ora",
    dataguard_member_names => $dg_hosts,
    dataguard_service_name => $service_name,
    dataguard_db_port      => $oradb_port,
    tns_entries            => hiera('oracle::tns_entries',[]),
  }


}
